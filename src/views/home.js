import React, { Component } from "react";
import "./home.css";

// Importing Home Component
import Header from "../components/home/header";
import About from "../components/home/about";
import Content from "../components/home/content";
import Footer from "../components/home/footer";

class Home extends Component {
  render() {
    return (
      <main className="Home">
        <Header />
        <About />
        <Content />
        <Footer />
      </main>
    );
  }
}
export { Home };

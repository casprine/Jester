import React from "react";
import { Switch, Route } from "react-router-dom";

// Page imports
import { Home } from "../views/home";

const Root = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Home} />
      </Switch>
    </div>
  );
};

export default Root;

import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import "./css/about.css";

class About extends PureComponent {
  render() {
    return (
      <div className="about">
        <div className="data center">
          All your favorite books <br />
          in one place
        </div>
        <div className="about-button center">
          <Link to="/" className="link">
            Subscribe
          </Link>
        </div>
      </div>
    );
  }
}

export default About;

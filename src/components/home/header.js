import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import "./css/header.css";

class Header extends PureComponent {
  render() {
    return (
      <div className="header">
        <Link to="/" className="navbrand">
          <div>Jester</div>
        </Link>
        <div className="links">
          <Link to="/" className="link">
            About us
          </Link>
          <Link to="/" className="link">
            Team
          </Link>
        </div>

        <div className="auth-buttons">
          <Link to="/" className="button">
            Signup
          </Link>

          <Link to="/" className="button">
            Login
          </Link>
        </div>
      </div>
    );
  }
}
export default Header;

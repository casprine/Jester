import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import "./css/book.css";

class Book extends PureComponent {
  state = { liked: false };

  handleClick = event => {
    this.setState({ liked: !this.state.liked });
  };

  render() {
    let likedStatus = this.state.liked ? "1" : "0";
    return (
      <div className="book">
        <div className="book-about">
          <Link to="./" className="link">
            <div className="book-title">{this.props.title || "Book Title"}</div>
          </Link>
        </div>
        <div className="book-author">{this.props.author || "Author"}</div>

        <div className="book-footer center">
          <div className="like">
            <ion-icon name="heart-empty" onClick={this.handleClick} />
            <div className="counter"> {likedStatus}</div>
          </div>
          <div className="category"> #{this.props.category || "Category"} </div>
        </div>
      </div>
    );
  }
}
export default Book;

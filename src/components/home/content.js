import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import Book from "./book";
import "./css/content.css";

class Content extends PureComponent {
  render() {
    return (
      <div className="content-wrapper">
        <div className="tabs center">
          <Link to="/" className="tab active">
            Javascript
          </Link>
          <Link to="/" className="tab">
            Next.js
          </Link>
          <Link to="/" className="tab">
            React
          </Link>
          <Link to="/" className="tab">
            Vue
          </Link>
        </div>

        <div className="content">
          <Book
            title="Eloquent Javascript - Edition 3"
            author="Marijn Haverbeke"
            category="javascript"
          />

          <Book
            title="You don't know javascript - Edition 2"
            author="Kyle Simpson"
            category="Javascript"
          />

          <Book
            title="React for absolute beginners"
            author="Wes Bos"
            category="react"
          />

          <Book
            title="Hands on Next.js"
            author="Christian Nwamba"
            category="next.js"
          />

          <Book
            title="Typescript 2.x for Angular Developers"
            author="Christian Nwamba"
            category="Typescript"
          />

          <Book
            title="Web Design for complete beginners"
            author="Christian Assempah"
            category="productivity"
          />
        </div>
      </div>
    );
  }
}

export default Content;

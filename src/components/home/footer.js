import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import "./css/footer.css";

class Footer extends PureComponent {
  render() {
    return (
      <footer>
        <div className="footer-about">
          Jester <br />
          <span className="copyright"> &copy; 2018</span>
        </div>

        <div className="ref">
          <Link to="/" className="ref-Link">
            About
          </Link>
          <Link to="/" className="ref-Link">
            Contact
          </Link>
          <Link to="/" className="ref-Link">
            Roadmap
          </Link>
        </div>
        <div className="ref">
          <Link to="/" className="ref-Link">
            About
          </Link>
          <Link to="/" className="ref-Link">
            Contact
          </Link>
          <Link to="/" className="ref-Link">
            Roadmap
          </Link>
        </div>
      </footer>
    );
  }
}
export default Footer;
